# README for LeafJ

## Welcome

Welcome to the LeafJ download site.  LeafJ is a plugin for [ImageJ](http://imagej.net/Welcome) that aids in the measurement of plant leafs.  Compared to other leaf measurement tools LeafJ is able to determine the border between the leaf blade and petiole, and return length measurements for both.  LeafJ also returns blade area, perimeter, and circularity.

LeafJ was written by [Julin Maloof](http://malooflab.openwetware.org).  

## Download

Download the latest version by clicking on the [Downloads tab](https://bitbucket.org/jnmaloof/leafj/downloads) and selecting the latest file (currently Version 1.3.5).

## Installation

Place the download .jar file in your ImageJ Plugins folder and restart ImageJ

## Citation

[Maloof et al., JOVE 2013](http://www.ncbi.nlm.nih.gov/pubmed/23380664)

## Contribute

Source file are also available on this site.  Create a new fork and make it better!

## Instructions for use:

LeafJ is designed to work on 8-bit (grayscale), thresholded images. If you have an RGB image you need to convert it. See item # 2, below, for my advice on converting.

LeafJ only works if the blade is higher than the petiole. If the leafs are in the opposite orientation then you can select a box containing the upside down leaves and then use Image>Transform> Flip Vertically to turn them right-side-up.

1. Open an image in imageJ
2. If the image is an RGB image, I have found that the most informative channel is the blue channel. To obtain the blue channel, choose Image>Color>Split Channels. This will give you three grayscale images from your original color file. Select the (blue) one.
3. Threshold the image. Press CMD-SHIFT-T (on the mac) or select Image>Adjust>Threshold. I like setting the display (right-hand pulldown) to Over/ Under. Be sure that "Dark background" is NOT checked. Adjust the slider until your leaves are shown and the background is green. You may need to adjust this separately for each genotype on your scan.  *Do not press the `apply` button in the threshold tool.  You can press `set` or just leave the threshold window open.
4. Use the rectangular selection tool to select an area containing the leafs that you want to measure. I find it convenient to measure 1 box of leaves at a time.
5. Select Plugins>LeafJ.￼￼￼
6. A "sample description" dialog box will be shown where you can select values that describe this sample. If you want to change the values and categories click on "Edit these options"
     1. If you clicked on "Edit these options" you will be presented with a spreadsheet-like editor. Each column represents one description category, with the rows providing possible values. These values will be displayed in a pull-down menu in the "sample description" dialog box. If you want to create a category where there is a text entry field then have a column with all empty rows. Click "done" when you are done editing. Any changes you make will be saved for the next time you use LeafJ.
7. Click "continue" in the Sample Description. A region of interest (ROI) will be created for each blade and petiole.
8. Click on the ROI manager. You can check to make sure that the ROIs are defined correctly. Delete any ROIs that you do not want measured. You can edit the petiole ROI by dragging the square boxes. To remove part of a blade ROI hold down the option key while making a selection using any of the ImageJ selection tools. To increase a blade ROI, hold down the shift key wile making a selection.
9. Click "OK" in the "Action Required" dialog box.
10. go back to step 3 or 4.
11. When you are done with the image, save the results file, or cut and paste into excel.

## Version History

### November 25, 2018

Version 1.3.5: 
* Updated to be compatible with imageJ 2.0.0-rc-54/1.52h 
* Allow user to modify minimum particle size of leaves 
TODO: make modified minParticleSize persist.

### July 22, 2015

Version 1.3.4: recompiled to be compatible with java 6 (should also work on java 7 and java 8)

Version 1.3.2: lowered circularity requirement because some leaves were getting missed.

## June 30,2012
Release notes for LeafJ 1.3

Fixed several bugs.

## June 25, 2012
Release notes for LeafJ_1.2

Fixed null pointer exceptions when user failed to draw an ROI or when user-drawn ROI bisected a leaf.

## June 23, 2012
Release Notes for LeafJ_1.1

Added ability for the user to edit the sample description table.

Known Issues: LeafJ will fail if the user does not select an area for measurement or if the selected area bisects a leaf.

## December 20, 2011
Release Notes for leaf_multi-alpha05

Minor bug fix

## November 17, 2011
Release Notes for leaf_multi-alpha04

Minor bug fix. Having the "global" box checked in the scale dialog box no longer results in plug-in crash.

## November 11, 2011
Release Notes for leaf_multi-alpha03

This version implements many requested features and also fixes several bugs

### Bug Fixes:
- No longer crosses over from one petiole to the next even when the leaves are
closely packed.
- Is better at not being confused by small bumps along the petiole
### New Features:
- Implemented a dialog box where the user can describe the sample that is about to be measured.
    - User selects from pre-defined lists of genotype, set, treatment.
    - Filename is automatically included.
    - Set, measured by, and dissected by fields are remembered for next time.
    - ROIs are (optionally) saved.
    - Filename is the same as the image filename + gt,trt,rep.
    - Can prevent saving by unchecking checkbox at user dialog box.
- The plug-in pauses after defining the ROis.
The user can adjust, and then press the OK button in the dialog box that comes up.
(You should no longer press the "measure" button on the ROI manager).
- Results are now displayed in a custom format.
    - Includes sample description data
    - Title matches image filename
    - New results are appended to this window until you switch to a new image file.  This means that you can process an entire image and then save the results at the end.
- Results window is brought to foreground at end.

### To do:
-Allow use of color thresholded image.
-Eliminate empty columns if not all sample description fields are filled out. -Change implementation of sample description to HashMap (internal change). -Allow user to change sample description pre-sets (low priority; hard).

## October 30, 2011
￼￼￼￼
Release notes for leaf_multi-alpha02
This updated version now automatically sorts leafs along the horizontal axis so that they are numbered 1..n from left to right.

## October 27, 2011
Release notes for leaf_multi-alpha01

This imageJ plug-in is designed to measure petiole length, and blade width, length, and area.

To install leaf_multi-alpha01, drop the leaf_multi-alpha01.jar file into your imageJ plug-ins folder.

This plug-in is designed to work on 8-bit (grayscale), thresholded images. If you have an RGB image you need to convert it. See below for my advice on converting. (I may change the plugin to work with color thresholded images if that would be helpful).

The plugin only works if the blade is higher than the petiole. If the leafs are in the opposite orientation then you can select a box containing the upside down leaves and then use Image>Transform> Flip Vertically to turn them right-side-up.